import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class LoginService {
  uri = environment.apiUrl;
  secureUri = environment.secureUri;
  constructor(private http: HttpClient) {
}
  login() {
    window.location.href = `${this.secureUri}/auth/microsoft/login`;
    return this.http.get(`${this.secureUri}/auth/microsoft/login`);
  }
  obtenerHeaders() {
    const session = JSON.parse(localStorage.getItem('cookieFanaia'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: session.token
      })
    };
    return httpOptions;
  }
}
