import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { desencriptarJson } from '../../../../assets/JS/desencrypt.js';
import { environment } from 'src/environments/environment';
import { async } from 'q';
import { LoginService } from '../../../services/login';
import { from } from 'rxjs';

@Component({
  selector: 'app-cabecera',
  templateUrl: './cabecera.component.html',
  styleUrls: ['./cabecera.component.scss']
})
export class CabeceraComponent implements OnInit {
  title = 'cliente-angular';
  identity = false;
  authentication = environment.authentication;
  userName = '';
  verResultadosCuestionarios = false;
  llenarCuestionarios = false;
  token = '';
  errorType = 0;
  btnReiniciar = false;
  encuesta = '';
  session: any;

  constructor(private router: Router, private login: LoginService) { }

  ngOnInit() {
    // Obtiene los datos de la URL.

    const query = window.location.search;
    const params = new URLSearchParams(query);
    // Verifica si en el ambiente necesita autenticación
    if (environment.authentication) {
      const contract = params.get('contract');
      if (localStorage.getItem('cookieFanaia')) {
      } else {
        if (contract == null && !localStorage.getItem('cookieFanaia')) {
          this.loginRedirect();
        } else {
          this.session = desencriptarJson(contract);

          localStorage.setItem('cookieFanaia', contract);
          this.router.navigate(['/']);
        }
      }

      if (localStorage.getItem('cookieFanaia')) {
        this.session = JSON.parse(localStorage.getItem('cookieFanaia'));
        this.identity = this.session.loggedIn;
        this.userName = this.session.name.split(' ')[0];
        this.validate_permissions(this.session);
        // tslint:disable-next-line: no-non-null-assertion
        this.token = this.session!.token;
      }
    } else {
      this.verResultadosCuestionarios = this.llenarCuestionarios = true;
      this.session = {
        name: 'Test_User',
        user_name: 'test_user@aportesenlinea.com',
        rols: ['LlenarCuestionarios', 'VerResultadosCuestionarios', 'Indicadores'],
        loggedIn: true,
        token: '',
      };
      this.identity = this.session.loggedIn;
      this.userName = this.session.name;
    }
  }

  loginRedirect() {
    this.login.login().subscribe(() => {
      this.router.navigate([`${this.login.secureUri}/auth/microsoft/login`]);
    });
  }
  logout() {
    localStorage.removeItem('cookieFanaia');
    window.location.href = '/';
  }
  cuestionario() {
    window.location.href = '/';
  }
  results() {
    window.location.href = '/tabla';
  }
  validate_permissions(session: any) {
    if (session) {
      const rols = session.rols;
      if (rols.includes('VerResultadosCuestionarios')) {
        this.verResultadosCuestionarios = this.llenarCuestionarios = true;
      } else if (rols.includes('LlenarCuestionarios')) {
        this.llenarCuestionarios = true;
      }
    }
    // tslint:disable-next-line: one-line
    else if (!environment.authentication) {
      this.verResultadosCuestionarios = this.llenarCuestionarios = true;
    }
  }
}

