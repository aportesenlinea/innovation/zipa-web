import { Component, OnInit } from '@angular/core';
import { CabeceraComponent } from '../../../../utils/components/cabecera/cabecera.component';

@Component({
  selector: 'app-caso-error',
  templateUrl: './caso-error.component.html',
  styleUrls: ['./caso-error.component.scss']
})
export class CasoErrorComponent implements OnInit {
  errorMessage = 'Ha ocurrido un error.';
  constructor(private cabecera: CabeceraComponent) {}

  ngOnInit() {

    // const query = window.location.search;
    // const params = new URLSearchParams(query);
    // const contract = params.get('errorId');
    switch (this.cabecera.errorType) {
      case 1:
        this.errorMessage =
          'El usuario no esta autorizado para utilizar el módulo seleccionado.';
        break;
      case 2:
        this.errorMessage = 'El usuario debe estar registrado';
        break;
      default:
        break;
    }
    return null;
  }
}
