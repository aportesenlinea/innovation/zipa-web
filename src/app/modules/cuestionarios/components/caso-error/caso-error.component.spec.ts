import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasoErrorComponent } from './caso-error.component';

describe('CasoErrorComponent', () => {
  let component: CasoErrorComponent;
  let fixture: ComponentFixture<CasoErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CasoErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasoErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
