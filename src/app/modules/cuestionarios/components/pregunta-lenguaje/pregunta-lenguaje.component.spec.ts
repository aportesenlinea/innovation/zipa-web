import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreguntaLenguajeComponent } from './pregunta-lenguaje.component';

describe('PreguntaLenguajeComponent', () => {
  let component: PreguntaLenguajeComponent;
  let fixture: ComponentFixture<PreguntaLenguajeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreguntaLenguajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreguntaLenguajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
