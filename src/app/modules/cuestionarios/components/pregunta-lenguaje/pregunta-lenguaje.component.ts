import { Component, OnInit, Self } from '@angular/core';
import { CuestionarioService } from '../../services/cuestionario.service';
import { Respuesta, Pregunta, Lenguaje } from '../../services/cuestionario.model';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { CuestionarioComponent } from '../cuestionario/cuestionario.component';
import { CabeceraComponent } from '../../../../utils/components/cabecera/cabecera.component';

@Component({
  selector: 'app-pregunta-lenguaje',
  templateUrl: './pregunta-lenguaje.component.html',
  styleUrls: ['./pregunta-lenguaje.component.scss']
})
export class PreguntaLenguajeComponent implements OnInit {

  lenguajes: any = [];
  displayedColumns = ['lenguaje', '_id', 'interes'];
  preguntas: any = [];
  respuestas: any = [];

  constructor(
    private cuestionarioService: CuestionarioService,
    private router: Router,
    private pre: CuestionarioComponent,
    private cabecera: CabeceraComponent) {
    this.preguntas = pre.PreguntaLenguaje();
    const self = this;

    pre.Lenguajes().then((lenguaje) => {
      const inputsArray = lenguaje.map((p) => ({
        key: 'respuesta' + p._id,
        value: ['', [Validators.required, Validators.min(0), Validators.max(100)]]
      }));
      const inputsObj = inputsArray.reduce((obj, item) => Object.assign(obj, { [item.key]: item.value }), {});
      self.lenguajes = lenguaje;

      pre.SolucionIngeniero().then((solucion) => {
        const respuestas = solucion[0].respuestas.filter((p) => p.tipo === 'LENGUAJE');
        // tslint:disable-next-line
        const inputsArray = respuestas.map((p) => ({
          key: 'respuesta' + p._id,
          value: ['', [Validators.required, Validators.min(0), Validators.max(100)]]
        }));
        // tslint:disable-next-line
        const inputsObj = inputsArray.reduce((obj, item) => Object.assign(obj, { [item.key]: item.value }), {});
        self.respuestas = respuestas;
        console.log(this.lenguajes);
        // tslint:disable-next-line
        this.lenguajes.forEach(lenguaje => {
          this.respuestas.forEach(respuesta => {
            if (lenguaje._id === respuesta.lenguaje) {
              lenguaje.experiencia = respuesta.experiencia;
              lenguaje.interesa = respuesta.interesa;
            }
          });
        });
      });
    });
  }

  addLenguaje(lenguajes) {
    console.log(lenguajes);
    this.pre.addRespuestaLenguaje(lenguajes);
  }

  ngOnInit() {
  }

}
