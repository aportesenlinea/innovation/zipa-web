import { Component, OnInit } from '@angular/core';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormBuilder, FormGroup, Validators, FormsModule, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NgModule } from '@angular/core';
import { MatInputModule, MatButtonModule, MatProgressBarModule } from '@angular/material';
import { CuestionarioComponent } from '../cuestionario/cuestionario.component';
import { CabeceraComponent } from '../../../../utils/components/cabecera/cabecera.component';





@Component({
  selector: 'app-pregunta-experiencia',
  templateUrl: './pregunta-experiencia.component.html',
  styleUrls: ['./pregunta-experiencia.component.scss']
})
export class PreguntaExperienciaComponent implements OnInit {
  public experienciaForm: FormGroup;
  selector: any;
  preguntasTemporal: any = [];
  preguntas: any = [];
  respuestas: any = [];

  arreglo = ['¿Cuantos años?', '¿Cuantos Lenguajes?', 'Edad'];
  pregun = '¿cuantos ?';

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private pre: CuestionarioComponent,
    private cabecera: CabeceraComponent
  ) {

    this.experienciaForm = new FormGroup({
      control: new FormControl()
    });

    const self = this;
    pre.Cuestionario().then((cuestionario) => {
      const preguntas = cuestionario[0].preguntas.filter((p) => p.tipo === 'EXPERIENCIA');
      const inputsArray = preguntas.map((p) => ({
        key: 'pregunta' + p._id,
        value: ['', [Validators.required, Validators.min(0), Validators.max(100)]]
      }));
      const inputsObj = inputsArray.reduce((obj, item) => Object.assign(obj, { [item.key]: item.value }), {});
      self.experienciaForm = self.fb.group(inputsObj);
      self.preguntas = preguntas;
    });
    pre.SolucionIngeniero().then((solucion) => {
      const respuestas = solucion[0].respuestas.filter((p) => p.tipo === 'EXPERIENCIA');
      const inputsArray = respuestas.map((p) => ({
        key: 'respuesta' + p._id,
        value: ['', [Validators.required, Validators.min(0), Validators.max(100)]]
      }));
      const inputsObj = inputsArray.reduce((obj, item) => Object.assign(obj, { [item.key]: item.value }), {});
      self.respuestas = respuestas;

      if (this.respuestas) {
        this.preguntas.forEach(pregunta => {
          this.respuestas.forEach(respuesta => {
            if (pregunta._id === respuesta.pregunta) {
              pregunta.experiencia = respuesta.experiencia;
            }
          });
        });
      }
    });

  }

  ngOnInit() {
  }

  hasError(controlName: string, errorName: string): boolean {
    return this.experienciaForm.controls[controlName].hasError(errorName);
  }

  trackByIndex(index: number, obj: any): any {
    return index;
  }
  addRespuesta(preguntas) {
    this.pre.addRespuestaExperiencia(preguntas);
  }
}
