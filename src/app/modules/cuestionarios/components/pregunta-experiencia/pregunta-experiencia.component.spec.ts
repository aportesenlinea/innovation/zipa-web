import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreguntaExperienciaComponent } from './pregunta-experiencia.component';
import { RouterModule, Routes } from '@angular/router';

describe('PreguntaExperienciaComponent', () => {
  let component: PreguntaExperienciaComponent;
  let fixture: ComponentFixture<PreguntaExperienciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PreguntaExperienciaComponent]
    })
      .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(PreguntaExperienciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
