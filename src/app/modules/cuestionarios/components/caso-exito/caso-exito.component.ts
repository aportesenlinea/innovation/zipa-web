import { Component, OnInit } from '@angular/core';
import { CabeceraComponent } from '../../../../utils/components/cabecera/cabecera.component';
@Component({
  selector: 'app-caso-exito',
  templateUrl: './caso-exito.component.html',
  styleUrls: ['./caso-exito.component.scss']
})
export class CasoExitoComponent implements OnInit {
  verResultadosCuestionarios = false;
  constructor(private cabecera: CabeceraComponent) {}

  ngOnInit() {
    this.verResultadosCuestionarios = this.cabecera.verResultadosCuestionarios;
  }
}
