import { Component, OnInit, NgModule, Pipe, PipeTransform, ChangeDetectionStrategy, ViewEncapsulation, OnChanges } from '@angular/core';
import { CuestionarioService } from '../../services/cuestionario.service';
import { Modulo, ModuleTree } from '../../services/cuestionario.model';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { CuestionarioComponent } from '../cuestionario/cuestionario.component';
import { ConfirmDialogModel, ConfirmDialogComponent } from '../../components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { CabeceraComponent } from '../../../../utils/components/cabecera/cabecera.component';
import { BindSunburst } from '../../../../../assets/JS/sunburst.js';

@Component({
  selector: 'app-pregunta-modulo',
  templateUrl: './pregunta-modulo.component.html',
  styleUrls: ['./pregunta-modulo.component.scss']
})
export class PreguntaModuloComponent implements OnInit {

  trabajado = false;
  interes = true;
  capacitacion = false;
  preguntas: string[];
  panelOpenState = false;
  searchText: string;
  modules: any;
  modulesLeafs: any = [];
  test: any;
  public searching = false;
  modulos: any = [];
  modulosFilter: any = [];
  respuestas: any = [];
  legend: boolean;
  allCapacitacion = false;
  allTrabajo = false;
  allInteres = false;
  treeModules: any;
  chart: any;
  loading = true;

  constructor(
    private cuestionarioService: CuestionarioService,
    private pre: CuestionarioComponent,
    public dialog: MatDialog,
    private cabecera: CabeceraComponent) {
    this.preguntas = pre.PreguntaModulo();
    const self = this;
    pre.Modulos().then((modulo) => {
      const inputsArray = modulo.map((p) => ({
        key: 'respuesta' + p._id,
        value: ['', [Validators.required, Validators.min(0), Validators.max(100)]]
      }));
      const inputsObj = inputsArray.reduce((obj, item) => Object.assign(obj, { [item.key]: item.value }), {});
      self.modulos = modulo;
      pre.SolucionIngeniero().then((solucion) => {
        const respuestas = solucion[0].respuestas.filter((p) => p.tipo === 'MODULO');
        // tslint:disable-next-line
        const inputsArray = respuestas.map((p) => ({
          key: 'respuesta' + p._id,
          value: ['', [Validators.required, Validators.min(0), Validators.max(100)]]
        }));
        // tslint:disable-next-line
        const inputsObj = inputsArray.reduce((obj, item) => Object.assign(obj, { [item.key]: item.value }), {});
        self.respuestas = respuestas;
        // tslint:disable-next-line
        this.respuestas.forEach(respuesta => {
          // tslint:disable-next-line
          this.modulos.forEach(modulo => {
            if (modulo._id === respuesta.modulo) {
              modulo.interesa = respuesta.interesa;
              modulo.conoce = respuesta.conoce;
              modulo.capacitado = respuesta.capacitado;
            }
          });
        });
      });
    });
    this.fetchModulos();
  }



  public showSearchResults(event: any): void {
    if (event.target.value.length >= 3) {
      this.searching = true;
    } else {
      this.searching = false;
    }
  }

  fetchModulos() {
    const self = this;

    this.cuestionarioService
      .getModuleTree()
      .then((data: ModuleTree[]) => {
        self.modules = data;
        self.BindModulesSunburst(self);
      });
  }

  mergeModules() {
    const self = this;
    self.getLeafsModules(self.treeModules, self.modulesLeafs);

    self.modulesLeafs.forEach(e => {
      const searchText = self.getModuleName(e.mod.props.nombre, e);

      e.fullPath = searchText;

      const m = self.modulos.find(x => x.nombre.toLowerCase().trim() === (searchText.toLowerCase().trim()));

      e.modulo = m;
    });
    self.loading = false;
  }

  getLeafsModules(mod: any, newModules: any[]) {
    if (mod.children !== undefined) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < mod.children.length; i++) {
        this.getLeafsModules(mod.children[i], newModules);
      }
    } else {
      newModules.push(mod);
    }
  }

  BindModulesSunburst(self) {
    // tslint:disable-next-line: prefer-const
    let selfdata = {
      click(s, e) {
        s.searchText = e.data.depth === 0 ? '' : s.getModuleName(e.data.mod.props.nombre, e.data);
        s.searching = e.data.depth !== 0;
      },
      data: self.modules,
      self
    };
    BindSunburst(selfdata);
  }

  ValidadorInteres(item) {
    let validador = false;
    this.modulos.forEach(element => {
      if (item === element.nombre) {
        if (element.interesa) {
          validador = true;
        }
      }
    });
    this.ValidadorAllInteres();
    return validador;
  }
  ValidadorTrabajo(item) {

    let validador = false;
    this.modulos.forEach(element => {
      if (item === element.nombre) {
        if (element.conoce) {
          validador = true;
        }
      }
    });
    this.ValidadorAllTrabajo();
    return validador;
  }
  ValidadorCapacitacion(item) {

    let validador = false;
    this.modulos.forEach(element => {
      if (item === element.nombre) {
        if (element.capacitado) {
          validador = true;
        }
      }
    });
    this.ValidadorAllCapacitacion();
    return validador;
  }
  addModulo(modulos, redirect = true) {

    modulos.forEach(modulo => {
      if (!modulo.interesa) {
        modulo.interesa = false;
      }
      if (!modulo.conoce) {
        modulo.conoce = false;
      }
      if (!modulo.capacitado) {
        modulo.capacitado = false;
      }
    });

    const self = this;

    if (redirect) {
      const message = `¿Deseas enviar las respuestas seleccionadas o quieres seguir respondiendo?`;
      const dialogData = new ConfirmDialogModel('Enviar respuestas', message, self);

      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        maxWidth: '80vw',
        maxHeight: '80vh',
        data: dialogData
      });

      dialogRef.afterClosed().subscribe(dialogResult => {
        if (dialogResult) {
          this.pre.addRespuestaModulo(modulos, redirect);
        }
      });
    } else {
      this.loading = true;
      this.pre.addRespuestaModulo(modulos, redirect);
    }
  }

  showAnswer() {
    alert(this.test.data.name);
  }

  getModuleName(name, mod) {
    if (mod.padre !== mod.key) {
      return this.getModuleName(mod.mod.props.padre.nombre + '/' + name, mod.parent);
    } else {
      return name.replace(mod.mod.props.padre.nombre + '/', '').split(' ').join('_');
    }
  }

  ngOnInit() {
  }

  onSelection(e, element, tipo) {
    // tslint:disable-next-line
    for (let modulo of this.modulos) {
      if (element === modulo.nombre) {
        if (e.checked === true) {
          if (tipo === 'interes') {
            modulo.interesa = true;
          } else if (tipo === 'trabajo') {
            modulo.conoce = true;
          } else if (tipo === 'capacitacion') {
            modulo.capacitado = true;
          }
        } else {
          if (tipo === 'interes') {
            modulo.interesa = false;
          } else if (tipo === 'trabajo') {
            modulo.conoce = false;
          } else if (tipo === 'capacitacion') {
            modulo.capacitado = false;
          }
        }
      }
    }
    this.ValidadorAllCapacitacion();
    this.ValidadorAllInteres();
    this.ValidadorAllTrabajo();
  }

  onSelectionFilter(e, element, tipo) {
    // tslint:disable-next-line
    for (let modulo of this.modulosFilter) {
      if (element._id === modulo._id) {
        if (e.checked === true) {
          if (tipo === 'interes') {
            modulo.interesa = true;
          } else if (tipo === 'trabajo') {
            modulo.conoce = true;
          } else if (tipo === 'capacitacion') {
            modulo.capacitado = true;
          }
        } else {
          if (tipo === 'interes') {
            modulo.interesa = false;
          } else if (tipo === 'trabajo') {
            modulo.conoce = false;
          } else if (tipo === 'capacitacion') {
            modulo.capacitado = false;
          }
        }
      }
    }
  }

  filterModulos() {
    this.modulosFilter = this.modulos.filter(
      element => element.nombre.toLowerCase().trim().includes(this.searchText.toLowerCase().trim())
    );
  }

  onAllCapacitacion(e) {
    this.filterModulos();
    this.modulosFilter.forEach(element => {
      this.onSelectionFilter(e, element, 'capacitacion');
      this.ValidadorAllCapacitacion();
    });
  }

  onAllInteres(e) {
    this.filterModulos();
    this.modulosFilter.forEach(element => {
      this.onSelectionFilter(e, element, 'interes');
    });
    this.ValidadorAllInteres();
  }

  onAllTrabajo(e) {
    this.filterModulos();
    this.modulosFilter.forEach(element => {
      this.onSelectionFilter(e, element, 'trabajo');
    });
    this.ValidadorAllTrabajo();
  }

  ValidadorAllInteres() {
    this.filterModulos();
    this.allInteres = this.modulosFilter.filter(x => !x.interesa).length === 0;
    return this.allInteres;
  }

  ValidadorAllTrabajo() {
    this.filterModulos();
    this.allTrabajo = this.modulosFilter.filter(x => !x.conoce).length === 0;
    return this.allTrabajo;
  }

  ValidadorAllCapacitacion() {
    this.filterModulos();
    this.allCapacitacion = this.modulosFilter.filter(x => !x.capacitado).length === 0;
    return this.allCapacitacion;
  }

  onLegend(e) {
    this.legend = !this.legend;
  }
}

