import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreguntaModuloComponent } from './pregunta-modulo.component';

describe('PreguntaModuloComponent', () => {
  let component: PreguntaModuloComponent;
  let fixture: ComponentFixture<PreguntaModuloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreguntaModuloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreguntaModuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
