import { Component, OnInit } from '@angular/core';
import { CuestionarioService } from '../../services/cuestionario.service';
import { ConstantPool } from '@angular/compiler';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { CabeceraComponent } from '../../../../utils/components/cabecera/cabecera.component';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { LoginService } from 'src/app/services/login';
@Component({
  selector: 'app-cuestionario',
  templateUrl: './cuestionario.component.html',
  styleUrls: ['./cuestionario.component.scss']
})
export class CuestionarioComponent implements OnInit {
  preguntaModulo: any = [];
  cuestionario: any = [];
  preguntasExperiencia: any = [];
  preguntaLenguaje: any = [];
  respuestaIngenieroExperiencia: any = [];
  respuestaIngenieroLenguaje: any = [];
  respuestaIngenieroModulo: any = [];
  temporal: any = [];
  respuestaLenguaje: any = [];
  respuestaExperiencia: any = [];
  respuestaModulo: any = [];
  solucionIngeniero: any = [];
  cuestionarioPromise: Promise<any>;
  lenguajes: Promise<any>;
  modulos: Promise<any>;


  constructor(
    private cuestionarioService: CuestionarioService,
    private router: Router,
    private snackBar: MatSnackBar,
    private cabecera: CabeceraComponent
  ) {

  }

  fetchCuestionario() {
    const self = this;
    this.cuestionarioPromise = this.cuestionarioService.getCuestionario();
    this.cuestionarioPromise.then((data: any) => {
      this.cuestionario = data;
      this.temporal = this.cuestionario[0].preguntas;
      this.temporal.forEach(element => {
        if (element.tipo === 'EXPERIENCIA') {
          self.preguntasExperiencia.push(element);
        } else if (element.tipo === 'MODULO') {
          self.preguntaModulo.push(element);
        } else if (element.tipo === 'LENGUAJE') {
          self.preguntaLenguaje.push(element);
        }
      });
    }, (e) => {
      this.snackBar.open(e.error.message, 'ERROR', {
        duration: 500000,
      });
      this.router.navigate(['caso-error']);
    }
    );
  }

  traerSolucionIngeniero() {
    const session = JSON.parse(localStorage.getItem('cookieFanaia'));
    this.solucionIngeniero = this.cuestionarioService.getSolucion(String(session.name));
    this.solucionIngeniero.then((data: any) => {
    });
  }

  traerLenguajes() {
    this.lenguajes = this.cuestionarioService.getLenguajes();
    this.lenguajes.then((data: any) => {
    });
  }

  traerModulos() {
    this.modulos = this.cuestionarioService.getModulos();
    this.modulos.then((data: any) => {
    });
  }

  Lenguajes() {
    return this.lenguajes;
  }
  Modulos() {
    return this.modulos;
  }
  SolucionIngeniero() {
    return this.solucionIngeniero;
  }
  Cuestionario() {
    return this.cuestionarioPromise;
  }
  PreguntaExperiencia() {
    return this.preguntasExperiencia;
  }
  PreguntaModulo() {
    return this.preguntaModulo;
  }
  PreguntaLenguaje() {
    return this.preguntaLenguaje;
  }

  ngOnInit() {
    if (!this.cabecera.llenarCuestionarios) {
      console.log('Error: Usuario no autorizado!');
      this.cabecera.errorType = 1;
      this.router.navigate(['/caso-error']);
    }
    this.fetchCuestionario();
    this.traerSolucionIngeniero();
    this.traerLenguajes();
    this.traerModulos();
  }

  addRespuestaExperiencia(respuesta) {
    this.respuestaExperiencia =
      respuesta.map(r => {
        const res = { ...r };
        if (!res.experiencia) {
          res.experiencia = 0;
        }
        res.pregunta = res._id;
        delete res._id;
        return res;
      });

    if (this.SolucionIngeniero().__zone_symbol__value.length > 0) {

      this.respuestaLenguaje.length = 0;
      this.respuestaModulo.length = 0;
      this.respuestaLenguaje = this.SolucionIngeniero().__zone_symbol__value[0].respuestas.filter((p) => p.tipo === 'LENGUAJE');
      this.respuestaModulo = this.SolucionIngeniero().__zone_symbol__value[0].respuestas.filter((p) => p.tipo === 'MODULO');
    }
    this.addRespuestas('experiencia');
    this.router.navigate(['cuestionario/pregunta-lenguaje']);
  }
  addRespuestaLenguaje(respuesta) {
    this.respuestaLenguaje =
      respuesta.map(r => {
        const res = { ...r };
        if (!res.interesa) {
          res.interesa = false;
        }
        if (!res.experiencia) {
          res.experiencia = 0;
        }
        res.lenguaje = res._id;
        res.tipo = 'LENGUAJE';
        res.pregunta = this.preguntaLenguaje[0]._id;
        delete res.nombre;
        delete res._id;
        return res;
      });
    if (this.SolucionIngeniero().__zone_symbol__value.length > 0) {
      this.respuestaModulo.length = 0;
      this.respuestaExperiencia.length = 0;
      this.respuestaExperiencia = this.SolucionIngeniero().__zone_symbol__value[0].respuestas.filter((p) => p.tipo === 'EXPERIENCIA');
      this.respuestaModulo = this.SolucionIngeniero().__zone_symbol__value[0].respuestas.filter((p) => p.tipo === 'MODULO');
    }
    this.addRespuestas('lenguaje');
  }
  addRespuestaModulo(respuesta, redirect = true) {
    this.respuestaModulo =
      respuesta.map(r => {
        const res = { ...r };
        res.tipo = 'MODULO';
        res.modulo = res._id;
        delete res.nombre;
        delete res._id;
        res.pregunta = this.preguntaModulo[0]._id;
        return res;
      });

    if (this.SolucionIngeniero().__zone_symbol__value.length > 0) {
      this.respuestaLenguaje.length = 0;
      this.respuestaExperiencia.length = 0;
      this.respuestaLenguaje = this.SolucionIngeniero().__zone_symbol__value[0].respuestas.filter((p) => p.tipo === 'LENGUAJE');
      this.respuestaExperiencia = this.SolucionIngeniero().__zone_symbol__value[0].respuestas.filter((p) => p.tipo === 'EXPERIENCIA');
    }
    this.addRespuestas('modulo', redirect);
  }

  addRespuestas(solicitado, redirect = true) {
    let respuestaFinal = this.respuestaExperiencia.concat(this.respuestaModulo);
    respuestaFinal = respuestaFinal.concat(this.respuestaLenguaje);
    // const session = JSON.parse(localStorage.getItem('cookieFanaia'));
    // tslint:disable-next-line
    var solucion = {
      persona: this.cabecera.session.name, // this.nombre,
      respuestas: respuestaFinal,
      cuestionario: this.cuestionario[0]._id,
    };
    this.cuestionarioService.addRespuesta(solucion).subscribe((a: any) => {
      this.snackBar.open(a.Mensaje, 'Ok', {
        duration: 5000,
      });
      this.traerSolucionIngeniero();
      if (solicitado === 'modulo') {
        if (redirect) {
          this.router.navigate(['caso-exito']);
        }
        // tslint:disable-next-line: deprecation
        setTimeout(() => { window.location.reload(true); }, 500);
      }
    }, (e) => {
      this.snackBar.open(e.error.message, 'ERROR', {
        duration: 5000,
      });
      if (solicitado === 'modulo') {
        if (redirect) {
          this.router.navigate(['caso-error']);
        }
        // tslint:disable-next-line: deprecation
        setTimeout(() => { window.location.reload(true); }, 500);
      }
    });
  }
}
