import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { BindPieChart } from '../../../../../assets/JS/piechart.js';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {
  title: string;
  message: string;
  parent: any;
  mInteresa: string;
  mConoce: string;
  mCapacitado: string;

  constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogModel) {
    // Update view with given values
    this.title = data.title;
    this.message = data.message;
    this.parent = data.self;

    const self = this;
    const modsInteresa = self.parent.modulos.filter(x => x.interesa).length;
    const modsConoce = self.parent.modulos.filter(x => x.conoce).length;
    const modsCapacitado = self.parent.modulos.filter(x => x.capacitado).length;
    const modsTotal = self.parent.modulos.length;

    const interesa = (modsInteresa / modsTotal) * 100;
    const conoce = (modsConoce / modsTotal) * 100;
    const capacitado = (modsCapacitado / modsTotal) * 100;

    this.mInteresa = modsInteresa + '/' + modsTotal + ' Módulos';
    this.mConoce = modsConoce + '/' + modsTotal + ' Módulos';
    this.mCapacitado = modsCapacitado + '/' + modsTotal + ' Módulos';

    BindPieChart({
      data: [
              { key: 'Si', y: interesa },
              { key: 'No', y: 100 - interesa },
            ],
      title: (interesa < 10 ? '0' : '') + interesa.toFixed(2) + '%',
      svgId: 'chInteresa',
      self
    });
    BindPieChart({
      data: [
              { key: 'Si', y: conoce },
              { key: 'No', y: 100 - conoce },
            ],
      title: (conoce < 10 ? '0' : '') + conoce.toFixed(2) + '%',
      svgId: 'chConoce',
      self
    });
    BindPieChart({
      data: [
              { key: 'Si', y: capacitado },
              { key: 'No', y: 100 - capacitado },
            ],
      title: (capacitado < 10 ? '0' : '') + capacitado.toFixed(2) + '%',
      svgId: 'chCapacitado',
      self
    });
  }

  onConfirm(): void {
    // Close the dialog, return true
    this.dialogRef.close(true);
  }

  onDismiss(): void {
    // Close the dialog, return false
    this.dialogRef.close(false);
  }
}

/**
 * Class to represent confirm dialog model.
 *
 * It has been kept here to keep it as part of shared component.
 */
export class ConfirmDialogModel {

  constructor(public title: string, public message: string, public self: any) {
  }
}
