import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CabeceraComponent } from '../../../../utils/components/cabecera/cabecera.component';
import { importType } from '@angular/compiler/src/output/output_ast';
import { CuestionarioComponent } from '../cuestionario/cuestionario.component';
@Component({
  selector: 'app-instruccion-inicial',
  templateUrl: './instruccion-inicial.component.html',
  styleUrls: ['./instruccion-inicial.component.scss']
})
export class InstruccionInicialComponent implements OnInit {

  constructor( private cabecera: CabeceraComponent, private cuestionario: CuestionarioComponent) { }

  ngOnInit() {
  }

  /*nombreUsuario(nom) {
    console.log(nom);
    this.cuestionario.nombre = nom;
  }*/

}
