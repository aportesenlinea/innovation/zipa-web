import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstruccionInicialComponent } from './instruccion-inicial.component';

describe('InstruccionesComponent', () => {
  let component: InstruccionInicialComponent;
  let fixture: ComponentFixture<InstruccionInicialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstruccionInicialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstruccionInicialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
