import { Component, OnInit } from '@angular/core';
import { CuestionarioService } from '../../services/cuestionario.service';
import { Router } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatInputModule,
  MatPaginatorModule,
  MatTableModule,
  MatSortModule
} from '@angular/material';
import { CabeceraComponent } from '../../../../utils/components/cabecera/cabecera.component';
import { CuestionarioComponent } from '../cuestionario/cuestionario.component';
import { PreguntaExperienciaComponent } from '../pregunta-experiencia/pregunta-experiencia.component';
import { global } from '../../../../../assets/JS/GLOBAL';

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.scss']
})
export class TablaComponent implements OnInit {
  preguntas: any = [];
  solucionesExperiencia: any = [];
  solucionesLenguaje: any = [];
  solucionesModulo: any = [];
  solucionesModuloFilter: any = [];
  lenguajes: any = [];
  modulos: any = [];
  modulosFilter: any = [];
  filtroNombre: string;
  filtroModulo: string;
  totalesModulo: any = [];
  totalPersonas: any = [];
  masModulosConoce: any = [];
  segundoMasModulos: any = [];
  btnBuscarPersona = true;
  btnBuscarModulo = true;
  existePersona = true;
  existeModulo = true;
  loading = true;


  constructor(
    private cuestionarioService: CuestionarioService,
    private router: Router,
    private pre: CuestionarioComponent,
    private cabecera: CabeceraComponent) {
    this.preguntas = pre.PreguntaExperiencia();
  }

  ngOnInit() {
    if (!this.cabecera.verResultadosCuestionarios) {
      console.log('Error: Usuario no autorizado!');
      this.cabecera.errorType = 1;
      this.router.navigate(['/caso-error']);
    }
    this.initValues();
  }

  error(self: any, e: any) {
    self.snackBar.open(e.error.message, 'ERROR', {
      duration: 500000,
    });
    console.log(e);
  }

  fetchTotalesModulos(self: any, data: any) {
    self.totalesModulo = data;
  }

  fetchTotalesPersona(self: any, data: any) {
    self.totalPersonas = data;
    // tslint:disable-next-line
    var temporal = {
      conoce: 0,
    };
    let temporal2;
    self.totalPersonas.forEach(element => {
      if (element.conoce >= temporal.conoce) {
        temporal2 = temporal;
        temporal = element;
        self.masModulosConoce = element;
        self.segundoMasModulos = temporal2;
      }
    });
  }

  fetchSolucion(self: any, data: any) {
    let contador = 0;
    let eliminar = [];
    self.solucionesExperiencia =
      data.map(r => {
        const res = { ...r };
        return res;
      });
    self.solucionesLenguaje =
      data.map(r => {
        const res = { ...r };
        return res;
      });

    self.solucionesModulo =
      data.map(r => {
        const res = { ...r };
        return res;
      });
    contador = 0;
    eliminar = [];
    self.solucionesExperiencia.forEach(solExp => {
      const exp = solExp.respuestas.filter((r) => r.tipo === 'EXPERIENCIA');
      solExp.respuestas = exp;
      if (solExp.respuestas.length > 1) {
        solExp.visible = true;
      } else {
        eliminar.push(contador);
      }
      contador++;
    });
    eliminar.forEach(elimi => {
      this.solucionesExperiencia.splice(elimi, 1);
    });
    contador = 0;
    eliminar = [];

    self.solucionesLenguaje.forEach(solLeng => {
      const exp = solLeng.respuestas.filter((r) => r.tipo === 'LENGUAJE');
      solLeng.respuestas = exp;

      if (solLeng.respuestas.length > 0) {
        solLeng.visible = true;
        solLeng.respuestas.forEach(respLeng => {
          if (respLeng.interesa === true) {
            respLeng.interesa = 'Si';
          } else {
            respLeng.interesa = 'No';
          }
        });
      } else {
        eliminar.push(contador);
      }
      contador++;
    });
    eliminar.forEach(elim => {
      this.solucionesLenguaje.splice(elim, 1);
    });
    contador = 0;
    eliminar = [];
    self.solucionesModulo.forEach(solMdl => {
      const exp = solMdl.respuestas.filter((r) => r.tipo === 'MODULO');
      const total = self.totalPersonas.find((e) => e.persona === solMdl.persona);

      solMdl.respuestas = exp;
      if (solMdl.respuestas.length > 0) {
        solMdl.visible = true;
        solMdl.capacitadoTotal = total.capacitado;
        solMdl.conoceTotal = total.conoce;
        solMdl.interesaTotal = total.interesa;

        solMdl.respuestas.forEach(respMdl => {
          if (solMdl.persona === self.masModulosConoce.persona) {
            solMdl.colorUsuario = 'danger';
            respMdl.color = 'light-danger';
          }
          if (solMdl.persona === self.segundoMasModulos.persona) {
            solMdl.colorUsuario = 'warning';
            respMdl.color = 'light-warning';
          }
          respMdl.visible = true;
          if (respMdl.interesa === true) {
            respMdl.interesa = 'Si';
          } else {
            respMdl.interesa = 'No';
          }
          if (respMdl.conoce === true) {
            respMdl.conoce = 'Si';
          } else {
            respMdl.conoce = 'No';
          }
          if (respMdl.capacitado === true) {
            respMdl.capacitado = 'Si';
          } else {
            respMdl.capacitado = 'No';
          }
          const totalMdl = self.totalesModulo.find((e) => e._id === respMdl.modulo);
          if (totalMdl) {
            if (totalMdl.conocen === 0) {
              if (solMdl.persona === self.segundoMasModulos.persona) {
                respMdl.color = 'light-alert';
              } else if (solMdl.persona === self.masModulosConoce.persona) {
                respMdl.color = 'light-double-danger';
              } else {
                respMdl.color = 'light-danger';
              }
            }
            if (totalMdl.conocen === 1) {
              if (solMdl.persona === self.masModulosConoce.persona) {
                respMdl.color = 'light-alert';
              } else if (solMdl.persona === self.segundoMasModulos.persona) {
                respMdl.color = 'light-double-warning';
              } else {
                respMdl.color = 'light-warning';
              }
            }
          }
        });
      } else {
        eliminar.push(contador);
      }
      contador++;
    });
    eliminar.forEach(elim => {
      this.solucionesModulo.splice(elim, 1);
    });

    self.solucionesModuloFilter = self.solucionesModulo;

    self.solucionesModulo.forEach(usuario => {
      usuario.respuestasFilter = usuario.respuestas;
    });
  }
  fetchLenguajes(self: any, data: any) {
    self.lenguajes = data;
  }

  fetchModulos(self: any, data: any) {
    self.modulos = data;
    self.modulosFilter = self.modulos = self.modulos.map(element => {
      const total = self.totalesModulo.find((e) => e._id === element._id);
      if (total) {
        if (total.conocen === 0) {
          element.color = 'danger';
        }
        if (total.conocen === 1) {
          element.color = 'warning';
        }
        element.capacitados = total.capacitados;
        element.conocen = total.conocen;
        element.interesan = total.interesa;
      }
      element.visible = true;
      return element;
    });
  }

  initValues() {
    const self = this;
    this.cuestionarioService.getTotalesModulo().then((data: any) => {
      self.fetchTotalesModulos(self, data);

      self.cuestionarioService.getTotalesPersona().then((data2: any) => {
        self.fetchTotalesPersona(self, data2);

        this.cuestionarioService.getSolucion(null).then((data3: any) => {
          self.fetchSolucion(self, data3);

          this.cuestionarioService.getLenguajes().then((data4: any) => {
            self.fetchLenguajes(self, data4);

            this.cuestionarioService.getModulos().then((data5: any) => {
              self.fetchModulos(self, data5);
              self.loading = false;
            }, (e) => { self.error(self, e); });
          }, (e) => { self.error(self, e); });
        }, (e) => { self.error(self, e); });
      }, (e) => { self.error(self, e); });
    }, (e) => { self.error(self, e); });
  }

  OcultarBotonPersona() {
    if (this.btnBuscarPersona === true) {
      this.btnBuscarPersona = false;
    } else {
      this.btnBuscarPersona = true;
    }
  }
  FuncionesBuscarPersona(nom) {
    this.filtroPersona(nom);
    this.OcultarBotonPersona();
  }
  quitarTilde(input) {
    // tslint:disable-next-line
    let tittles = "ÃÀÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàááááááäâèééééééëêìíïîòóóóóóóóóóóóöôùúüûÑñÇç";
    // tslint:disable-next-line
    let original = "AAAAAAAAAAAAAAAAAAAAAAAAAEEEEIIIIOOOOUUUUaaaaaaaaaaeeeeeeeeeiiiioooooooooooooouuuunncc";
    for (let i = 0; i < tittles.length; i++) {
      input = input.replace(tittles.charAt(i), original.charAt(i)).toLowerCase();
    }
    return input;
  }

  filtroPersona(nombre) {
    if (nombre !== undefined) {
      this.existePersona = false;
      this.solucionesExperiencia.forEach(element => {
        if (!this.quitarTilde(element.persona.toLowerCase()).includes(this.quitarTilde(nombre.toLowerCase()))) {
          element.visible = false;
        } else {
          element.visible = true;
          this.existePersona = true;
        }
      });

      this.solucionesLenguaje.forEach(element2 => {
        if (!element2.persona.toLowerCase().includes(nombre.toLowerCase())) {
          element2.visible = false;
        } else {
          element2.visible = true;
        }
      });

      this.solucionesModulo.forEach(element3 => {
        if (!element3.persona.toLowerCase().includes(nombre.toLowerCase())) {
          element3.visible = false;
        } else {
          element3.visible = true;
        }
      });
      this.solucionesModuloFilter = this.solucionesModulo.filter((e) => e.visible);
    }
  }
  OcultarBotonModulo() {
    if (this.btnBuscarModulo === true) {
      this.btnBuscarModulo = false;
    } else {
      this.btnBuscarModulo = true;
    }
  }
  FuncionesBuscarModulo(nom) {
    this.FiltroModulo(nom);
    this.OcultarBotonModulo();
  }

  FiltroModulo(nombre) {
    if (nombre !== undefined) {
      this.existeModulo = false;
      this.solucionesModulo.forEach(usuario => {
        this.modulos.forEach(element => {
          if (!this.quitarTilde(element.nombre.toLowerCase()).includes(this.quitarTilde(nombre.toLowerCase()))) {
            usuario.respuestas.forEach(solucion => {
              if (element._id === solucion.modulo) {
                solucion.visible = false;
              }

            });
            element.visible = false;
          } else {
            usuario.respuestas.forEach(solucion => {
              if (element._id === solucion.modulo) {
                solucion.visible = true;
              }
            });
            element.visible = true;
            this.existeModulo = true;
          }
          usuario.respuestasFilter = usuario.respuestas.filter((e) => e.visible);
        });
      });
      this.solucionesModuloFilter = this.solucionesModulo.filter((e) => e.visible);
      this.modulosFilter = this.modulos.filter((e) => e.visible);
    }
  }
}
