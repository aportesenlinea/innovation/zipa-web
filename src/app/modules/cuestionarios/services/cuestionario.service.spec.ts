import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { CuestionarioService } from './cuestionario.service';

describe('PreguntaService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: CuestionarioService = TestBed.get(CuestionarioService);
    expect(service).toBeTruthy();
  });
});
