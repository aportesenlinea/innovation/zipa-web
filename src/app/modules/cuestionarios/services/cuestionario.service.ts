import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CuestionarioService {
  uri = environment.apiUrl;
  constructor(private http: HttpClient) {
  }

  addRespuesta(respuesta) {
    if (environment.authentication) {
      const httpOptions = this.obtenerHeaders();
      return this.http.post(`${this.uri}/api/v1/cuestionarios/solucion`, respuesta);
    } else {
      return this.http.post(`${this.uri}/api/v1/cuestionarios/solucion`, respuesta);
    }
  }
  async getSolucion(ingeniero) {
    if (environment.authentication) {
      const httpOptions = this.obtenerHeaders();
      return this.http.get(`${this.uri}/api/v1/cuestionarios/solucion/${ingeniero}`).toPromise();
    } else {
      return this.http.get(`${this.uri}/api/v1/cuestionarios/solucion/${ingeniero}`).toPromise();
    }
  }
  async getTotalesModulo() {
    if (environment.authentication) {
      const httpOptions = this.obtenerHeaders();
      return this.http.get(`${this.uri}/api/v1/cuestionarios/totalesModulo`).toPromise();
    } else {
      return this.http.get(`${this.uri}/api/v1/cuestionarios/totalesModulo`).toPromise();
    }
  }
  async getTotalesPersona() {
    if (environment.authentication) {
      const httpOptions = this.obtenerHeaders();
      return this.http.get(`${this.uri}/api/v1/cuestionarios/totalesPersona`).toPromise();
    } else {
      return this.http.get(`${this.uri}/api/v1/cuestionarios/totalesPersona`).toPromise();
    }
  }
  async getCuestionario() {
    if (environment.authentication) {
      const httpOptions = this.obtenerHeaders();
      return await this.http.get(`${this.uri}/api/v1/cuestionarios`).toPromise();
    } else {
      return await this.http.get(`${this.uri}/api/v1/cuestionarios`).toPromise();
    }
  }
  async getLenguajes() {
    if (environment.authentication) {
      const httpOptions = this.obtenerHeaders();
      return this.http.get(`${this.uri}/api/v1/cuestionarios/lenguaje`).toPromise();
    } else {
      return this.http.get(`${this.uri}/api/v1/cuestionarios/lenguaje`).toPromise();
    }
  }
  async getModulos() {
    if (environment.authentication) {
      const httpOptions = this.obtenerHeaders();
      return this.http.get(`${this.uri}/api/v1/cuestionarios/modulo`).toPromise();
    } else {
      return this.http.get(`${this.uri}/api/v1/cuestionarios/modulo`).toPromise();
    }
  }
  async getModuleTree() {
    if (environment.authentication) {
      const httpOptions = this.obtenerHeaders();
      return this.http.get(`${this.uri}/api/v1/cuestionarios/moduleTree`).toPromise();
    } else {
      return this.http.get(`${this.uri}/api/v1/cuestionarios/moduleTree`).toPromise();
    }
  }
  obtenerHeaders() {
    const session = JSON.parse(localStorage.getItem('cookieFanaia'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        // tslint:disable-next-line: no-non-null-assertion
        Authorization: session!.token
      })
    };
    return httpOptions;
  }
}
