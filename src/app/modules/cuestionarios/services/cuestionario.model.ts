export interface Respuesta {
  respuesta: `string`;
  preguntaId: `string`;
}

export interface Pregunta {
  pregunta: `string`;
}
export interface Modulo {
  modulo: `string`;
}

export interface PreguntaRespuesta {
  pregunta: `string`;
  respuesta: `string`;
}

export interface RespuestaLenguaje {
  pregunta: `string`;
  respuesta: `string`;
}
export interface Lenguaje {
  Lenguaje: `string`;

}

export interface ModuleTree {
  key: `string`;
  padre: `string`;
  familia: `string`;
  name: `string`;
  mod: `object`;
  children: `object[]`;
}
