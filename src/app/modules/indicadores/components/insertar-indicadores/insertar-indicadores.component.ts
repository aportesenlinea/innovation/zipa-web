import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { MatSnackBar, MatInputModule, MatDatepicker, MatAutocompleteModule } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { importType, NONE_TYPE } from '@angular/compiler/src/output/output_ast';
import { MatTableModule } from '@angular/material/table';
import { IndicadoresService } from '../../Services/indicadores.service';
import { IngenierosService } from '../../Services/ingenieros.services';
import { from } from 'rxjs';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
import * as _rollupMoment from 'moment';
import { Moment } from 'moment';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { MatTableDataSource } from '@angular/material/table';


const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
    parse: {
        dateInput: 'MM/YYYY',
    },
    display: {
        dateInput: 'MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};
@Component({
    // tslint:disable-next-line
    selector: 'insertar-indicadores',
    templateUrl: './insertar-indicadores.component.html',
    styleUrls: ['./insertar-indicadores.component.scss'],
    providers: [
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
        },

        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    ],
})
export class InsertarIndicadoresComponent implements OnInit {
    controlCampoIngeniero = new FormControl();
    controlCampoIndicador = new FormControl();
    controlCampoValor = new FormControl();
    date: any;
    msjEror = '';
    msjExito = '';
    extension: any;
    uploadForm: FormGroup;
    file: any;
    loading = false;
    isLinear = false;
    monthCalculate: any;
    yearCalculate: any;
    mostarHistorial = false;
    fechaCalculo: any;
    ingenieros: string[] = [];
    ingenieroSel: string;
    // tslint:disable-next-line
    ingeniero: string = '';
    indicadorSel: string;
    indicadores: string[] = [];
    filteredOptions: Observable<string[]>;
    disableSelect = new FormControl(false);
    valorInd = false;
    guardarInd = false;
    objetoIngeniero: any;
    valorIndicador: any;
    mostrarTabla = true;
    indicadoresCargue = [];
    fecha: any;
    // tslint:disable-next-line
    valor_nuevo_indicador: Number;
    // tslint:disable-next-line
    searchValue: any;
    value: any;
    prueba: any;
    displayedColumns = [];
    constructor(
        private snackBar: MatSnackBar,
        private formBuilder: FormBuilder,
        private httpClient: HttpClient,
        private indicadoresServices: IndicadoresService,
        private ingenierosServices: IngenierosService,
    ) { }
    ngOnInit() {
        this.file = { name: 'Escoja un archivo Excel' };
        this.uploadForm = this.formBuilder.group({
            // tslint:disable-next-line
            upLoad: []
        });
        this.obtenerIndicadoresDisponibles();
        this.filteredOptions = this.controlCampoIngeniero.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
        );
        this.date = new FormControl(moment(null));
    }
    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.ingenieros.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }
    private mostrar_ingeniero() {
        alert('Hola');
    }
    // Consulta de paramétricos
    obtenerIngenieros() {
        this.ingenieros = [];
        if (this.monthCalculate == null && this.yearCalculate == null) {
            const fechaActual = new Date();
            this.fechaCalculo = {
                obj: {
                    month: fechaActual.getMonth() + 1,
                    year: fechaActual.getFullYear()
                }
            };
        } else {
            this.fechaCalculo = {
                obj: {
                    month: this.monthCalculate,
                    year: this.yearCalculate
                }
            };
        }
        this.ingenierosServices.obtenerListaIngenierosPorFecha(JSON.parse(JSON.stringify(this.fechaCalculo))).subscribe((res) => {
            this.ingenieros = res.ingenieros;
        }, (e) => {
            console.log(e);
        });
    }
    obtenerIndicadoresDisponibles() {
        let objeto;
        objeto = {
            obj: {
                tipo_ingreso: 'Manual'
            }
        };
        this.indicadoresServices.obtenerIndicadoresDisponibles(objeto).subscribe((data: any) => {
            this.indicadores = data;
        }, (e) => {
            console.log(e);
        });
    }
    // Controladores de botones
    chosenYearHandler(normalizedYear: Moment) {
        const ctrlValue = this.date.value;
        ctrlValue.year(normalizedYear.year());
        this.date.setValue(ctrlValue);
        this.yearCalculate = normalizedYear.year();
        this.ingenieros = [];
        this.ingeniero = '';
        this.ingenieroSel = null;
    }
    chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
        this.date = new FormControl(moment(normalizedMonth));
        const ctrlValue = this.date.value;
        ctrlValue.month(normalizedMonth.month());
        this.date.setValue(ctrlValue);
        datepicker.close();
        this.monthCalculate = normalizedMonth.month() + 1;
        this.ingenieros = [];
        this.ingeniero = '';
        this.ingenieroSel = null;
        const ing = document.getElementById('cabecera') as HTMLElement;
        ing.innerText = '';
        this.obtenerIngenieros();
    }
    guardarIngeniero(ingeniero) {
        this.ingenieroSel = ingeniero;
        this.consultarIndicadorUnicoIngeniero();
    }
    guardarIndicador(indicador) {
        this.indicadorSel = indicador;
        this.consultarIndicadorUnicoIngeniero();
    }
    guardarValorNuevo(event) {
        this.valor_nuevo_indicador = event.target.value;
        this.consultarIndicadorUnicoIngeniero();
    }
    // Consulta y almacenamiento del indicador
    consultarIndicadorUnicoIngeniero() {
        if (this.ingenieroSel != null && this.monthCalculate != null && this.yearCalculate != null && this.indicadorSel != null) {
            this.objetoIngeniero = {
                obj: {
                    ingeniero: this.ingenieroSel,
                    indicador: this.indicadorSel,
                    month: this.monthCalculate,
                    year: this.yearCalculate
                }
            };
            this.indicadoresServices.obtenerIndicadorUnicoIngeniero(this.objetoIngeniero).subscribe((res) => {
                this.valorIndicador = res.valor_indicador + ' %';
                this.valorInd = true;
                this.guardarInd = true;
            }, (e) => {
                this.valorIndicador = 0;
                this.valorInd = true;
                this.guardarInd = true;
            });
        } else {
            if (this.indicadorSel != null) {
                this.snackBar.open('Datos faltantes.', 'ERROR', {
                    duration: 5000,
                });
            }
        }
    }
    guardarIndicadorUnicoIngeniero() {
        this.controlCampoValor = new FormControl();
        if (
            this.ingenieroSel != null &&
            this.monthCalculate != null &&
            this.yearCalculate != null &&
            this.indicadorSel != null &&
            this.valor_nuevo_indicador != null
        ) {
            if (100 < this.valor_nuevo_indicador || this.valor_nuevo_indicador < 0) {
                this.snackBar.open('Numero no válido', 'ERROR', {
                    duration: 5000,
                });
                this.searchValue = null;
                return;
            }
            this.objetoIngeniero = {
                obj: {
                    ingeniero: this.ingenieroSel,
                    indicador: this.indicadorSel,
                    month: this.monthCalculate,
                    year: this.yearCalculate,
                    valor: this.valor_nuevo_indicador
                }
            };
            this.valor_nuevo_indicador = null;
            this.searchValue = undefined;
            this.indicadoresServices.guardarIndicadorUnicoIngeniero(this.objetoIngeniero).subscribe((res) => {
                this.searchValue = null;
                this.indicadoresCargue.push(this.objetoIngeniero.obj);
                this.mostarHistorial = true;
                this.displayedColumns = [
                    'Nombre',
                    'Indicador',
                    'Ponderado',
                    'Fecha'
                ];
                this.snackBar.open(res.message, 'Ok', {
                    duration: 5000,
                });
                this.controlCampoValor.setValidators(null);
                this.guardarInd = false;
                this.consultarIndicadorUnicoIngeniero();
            }, (e) => {
                this.snackBar.open('', 'ERROR', {
                    duration: 5000,
                });
            });
        } else {
            if (this.indicadorSel == null) {
                this.snackBar.open('Datos faltantes', 'ERROR', {
                    duration: 5000,
                });
            }
        }
    }
}
