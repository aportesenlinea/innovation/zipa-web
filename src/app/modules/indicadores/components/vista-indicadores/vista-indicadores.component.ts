import { Component, OnInit } from '@angular/core';
import { CabeceraComponent } from '../../../../utils/components/cabecera/cabecera.component';
import { TableUtil } from '../../../../utils/components/exportar-Excel/tableUtil';
import { MatSnackBar, MatInputModule, MatDatepicker, MatAutocompleteModule } from '@angular/material';
import { IndicadoresService } from '../../Services/indicadores.service';
import { MatTableModule } from '@angular/material/table';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
import * as _rollupMoment from 'moment';
import { Moment } from 'moment';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { IngenierosService } from '../../Services/ingenieros.services';
const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-vista-indicadores',
  templateUrl: './vista-indicadores.component.html',
  styleUrls: ['./vista-indicadores.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class VistaIndicadoresComponent implements OnInit {
  displayedColumns = [
    'Nombre',
    'Ponderado',
    'Indicador Bug',
    'Indicador Velocidad',
    'Indicador EPS',
    'Indicador Adherencia',
    'Indicador Estimación',
    'Indicador Ejecución',
    'Indicador ANS',
    'Indicador Mejora',
    'Indicador Proyectos',
  ];
  existeIndicador = true;
  myControl = new FormControl();
  obj: any = [];
  hoy = new Date();
  date = new FormControl(moment(null));
  indicadores = [];
  mostrar = false;
  mostrarTabla = false;
  mostrarOpcIngeniero = false;
  btnConsulta = false;
  searching = false;
  tamanoText = 0;
  yearCalculate: any;
  // tslint:disable-next-line
  ingeniero: string = '';
  // tslint:disable-next-line
  ingenieros: string[] = [];
  // tslint:disable-next-line
  ingeniero_sel: string = 'x';
  monthCalculate: any;
  filteredOptions: Observable<string[]>;
  opciones = [
    { name: 'Indicador total fabrica' },
    { name: 'Indicador ingeniero' },
    { name: 'Historial por ingeniero' },
  ];
  constructor(
    private cabecera: CabeceraComponent,
    private indicadoresServices: IndicadoresService,
    private ingenierosServices: IngenierosService
  ) { }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this.obtenerIngenieros();
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.ingenieros.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  obtenerIngenieros() {
    this.ingenieros = [];
    this.ingenierosServices.obtenerListaTotalIngenierosPorFecha().then((res) => {
      this.ingenieros = res.ingenieros;
    }, (e) => {
      console.log(e);
    });
  }

  obtenerResultados(objeto) {
    console.log(objeto);

    this.indicadoresServices.obtenerResultadoIndicadores(objeto).subscribe((data: any) => {
      this.obj = data;
      this.indicadores = data.indicadores;
      if (data.indicadores.length > 0) {
        this.mostrar = false;
        this.mostrarTabla = true;
        this.mostrarOpcIngeniero = true;
      } else {
        this.mostrarTabla = false;
        this.mostrar = true;
      }
    }, (e) => {
      this.mostrar = true;
    });
  }

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
    this.yearCalculate = normalizedYear.year();
  }
  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    this.date = new FormControl(moment(normalizedMonth));
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
    this.monthCalculate = normalizedMonth.month() + 1;
  }

  guardarIngeniero(ingeniero) {

    this.ingeniero_sel = ingeniero;
    ingeniero = '';
  }

  consulta_indicadores() {
    let objeto = {};
    console.log('este: ' + this.ingeniero_sel);

    if (this.searching === true && this.tamanoText === 0 && this.ingeniero_sel.length < 3) {
      this.ingeniero_sel = 'x';
      this.searching = false;
      this.tamanoText = + 1;
    }
    if (this.ingeniero_sel === 'x') {
      // tslint:disable-next-line
      objeto = {
        obj: {
          ingeniero: null,
          month: this.monthCalculate,
          year: this.yearCalculate
        }
      };
    } else {
      objeto = {
        obj: {
          ingeniero: this.ingeniero_sel,
          month: this.monthCalculate,
          year: this.yearCalculate
        }
      };
      this.btnConsulta = true;
    }
    this.ingeniero_sel = 'x';
    this.obtenerResultados(objeto);
  }

  limpiarBusqueda() {
    this.btnConsulta = false;
    this.searching = true;
    this.tamanoText = 0;
    this.myControl = new FormControl();
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this.obtenerIngenieros();
    this.consulta_indicadores();
  }
  exportTable() {
    const nombre = 'descarga:' + String(this.yearCalculate) + '_' +  String(this.monthCalculate);
    TableUtil.exportToExcel('tabla_total_fabrica', nombre);
  }
  public showSearchResults(event: any): void {
    this.tamanoText = event.target.value.length;
    console.log(event.target.value.length);
    if (this.tamanoText === 0) {
      this.searching = true;
    } else {
      this.searching = false;
    }
  }
}
