import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaIndicadoresComponent } from './vista-indicadores.component';

describe('VistaIndicadoresComponent', () => {
  let component: VistaIndicadoresComponent;
  let fixture: ComponentFixture<VistaIndicadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaIndicadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaIndicadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
