import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { MatSnackBar, MatInputModule, MatDatepicker } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { importType } from '@angular/compiler/src/output/output_ast';
import { IndicadoresService } from '../../Services/indicadores.service';
import { from } from 'rxjs';
import { Router } from '@angular/router';
import { CabeceraComponent } from '../../../../utils/components/cabecera/cabecera.component';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
import * as _rollupMoment from 'moment';
import { Moment } from 'moment';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-cargue-excel',
  templateUrl: './cargue-excel.component.html',
  styleUrls: ['./cargue-excel.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class CargueExcelComponent implements OnInit {
  date = new FormControl(moment(null));
  file: any;
  file2: any;
  extensiones: any = new Array('.xls', '.xslx', '.xlsx');
  msjEror = '';
  msjExito = '';
  extension: any;
  uploadForm: FormGroup;
  loading = false;
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  monthCalculate: any;
  yearCalculate: any;
  fechaCalculo: any;
  btnCargue = false;
  btnCargueCapacidad = false;
  btnSiguiente = false;

  constructor(
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private httpClient: HttpClient,
    private indicadoresServices: IndicadoresService,
    private router: Router,
    private cabecera: CabeceraComponent,
  ) { }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      this.btnCargue = true;
      this.file = event.target.files[0];
      this.uploadForm.get('upLoad').setValue(this.file);
    }
  }
  onFileChange2(event) {
    if (event.target.files.length > 0) {
      this.btnCargueCapacidad = true;
      this.file2 = event.target.files[0];
      console.log(this.file2);
      this.uploadForm.get('upLoad').setValue(this.file2);
    }
  }

  onSubmit() {
    this.loading = true;
    let permitida = false;
    if (!this.file.type) {
      this.msjEror = 'No se ha seleccionado ningún archivo';
      this.snackBar.open(this.msjEror, '', {
        duration: 4000,
        panelClass: ['mat-orange']
      });
      this.loading = false;
    } else {
      this.extension = (this.file.name.substring(this.file.name.lastIndexOf('.'))).toLowerCase();
      this.extensiones.forEach(element => {
        if (this.extension === element) {
          permitida = true;
        }
      });
      if (!permitida) {
        this.msjEror = 'error: tipo de archivo no permitido';
        this.snackBar.open(this.msjEror, '', {
          duration: 4000,
          panelClass: ['mat-orange']
        });
        this.loading = false;
      } else {
        const formData = new FormData();
        formData.append('file', this.uploadForm.get('upLoad').value);
        this.indicadoresServices.addArchivoRequerimiento(formData)
          .subscribe((res) => {
            this.btnSiguiente = true;
            this.snackBar.open(res.messege, 'Ok', {
              duration: 5000,
            });
            this.loading = false;
          }, (e) => {
            this.snackBar.open(e.error.messege, 'ERROR', {
              duration: 5000,
            });
            this.loading = false;
          });
      }
    }
  }

  onSubmit2() {
    this.loading = true;
    let permitida = false;
    if (!this.file2.type) {
      this.msjEror = 'No se ha seleccionado ningún archivo';
      this.snackBar.open(this.msjEror, '', {
        duration: 4000,
        panelClass: ['mat-orange']
      });
      this.loading = false;
    } else {
      this.extension = (this.file2.name.substring(this.file2.name.lastIndexOf('.'))).toLowerCase();
      this.extensiones.forEach(element => {
        if (this.extension === element) {
          permitida = true;
        }
      });
      if (!permitida) {
        this.msjEror = 'error: tipo de archivo no permitido';
        this.snackBar.open(this.msjEror, '', {
          duration: 4000,
          panelClass: ['mat-orange']
        });
        this.loading = false;
      } else {
        const formData = new FormData();
        formData.append('file', this.uploadForm.get('upLoad').value);
        this.indicadoresServices.addArchivoCapacidad(formData)
          .subscribe((res) => {
            this.btnSiguiente = true;
            this.snackBar.open(res.messege, 'Ok', {
              duration: 5000,
            });
            this.loading = false;
          }, (e) => {
            this.snackBar.open(e.error.messege, 'ERROR', {
              duration: 5000,
            });
            this.loading = false;
          });
      }
    }
  }

  ngOnInit() {
    this.file = {name: 'Escoja un archivo Excel'};
    this.file2 = {name: 'Escoja un archivo Excel'};
    this.uploadForm = this.formBuilder.group({
      upLoad: []
    });
    this.firstFormGroup = this.formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }
  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
    this.yearCalculate = normalizedYear.year();

  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    this.date = new FormControl(moment(normalizedMonth));
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
    this.monthCalculate = normalizedMonth.month() + 1;
  }

  CalcularIndicador() {
    this.loading = true;
    if (this.monthCalculate == null && this.yearCalculate == null) {
      const fechaActual = new Date();
      this.fechaCalculo = {
        obj: {
          month: fechaActual.getMonth() + 1,
          year: fechaActual.getFullYear()
        }
      };
    } else {
      this.fechaCalculo = {
        obj: {
          month: this.monthCalculate,
          year: this.yearCalculate
        }
      };
    }
    this.indicadoresServices.calcularIndicadores(this.fechaCalculo)
      .subscribe((res) => {
        this.btnSiguiente = true;
        this.snackBar.open(res.messege, 'Ok', {
          duration: 5000,
        });

        this.loading = false;

      }, (e) => {
        this.snackBar.open(e.error.messege, 'ERROR', {
          duration: 5000,
        });
        this.loading = false;
      });
  }

  cambiarBtn() {
    this.btnSiguiente = !this.btnSiguiente;
  }
}
