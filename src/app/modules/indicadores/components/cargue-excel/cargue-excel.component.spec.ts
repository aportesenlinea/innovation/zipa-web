import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargueExcelComponent } from './cargue-excel.component';

describe('CargueExcelComponent', () => {
  let component: CargueExcelComponent;
  let fixture: ComponentFixture<CargueExcelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CargueExcelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargueExcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
