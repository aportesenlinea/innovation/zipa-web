import { Component, OnInit } from '@angular/core';
import { CabeceraComponent } from '../../../../utils/components/cabecera/cabecera.component';
import { TableUtil } from '../../../../utils/components/exportar-Excel/tableUtil';
import { MatSnackBar, MatInputModule, MatDatepicker, MatAutocompleteModule } from '@angular/material';
import { IndicadoresService } from '../../Services/indicadores.service';
import { MatTableModule } from '@angular/material/table';
import { Moment } from 'moment';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { IngenierosService } from '../../Services/ingenieros.services';

@Component({
  selector: 'app-vista-historial-indicadores',
  templateUrl: './vista-historial-indicadores.component.html',
  styleUrls: ['./vista-historial-indicadores.component.scss'],
})
export class VistaHistorialIndicadoresComponent implements OnInit {
  displayedColumns = [
    'Nombre',
    'Ponderado',
    'Fechas',
    'Indicador Bug',
    'Indicador Velocidad',
    'Indicador EPS',
    'Indicador Adherencia',
    'Indicador Estimación',
    'Indicador Ejecución',
    'Indicador ANS',
    'Indicador Mejora',
    'Indicador Proyectos'
  ];
  existeIndicador = true;
  myControl = new FormControl();
  obj: any = [];
  indicadores = [];
  mostrar = false;
  mostrarTabla = false;
  // tslint:disable-next-line
  ingeniero: string = '';
  // tslint:disable-next-line
  ingenieros: string[] = [];
  // tslint:disable-next-line
  value: any;
   // tslint:disable-next-line
  ingeniero_sel: string = 'x';
  filteredOptions: Observable<string[]>;
  opcionesControl = new FormControl('', [Validators.required]);
  selectFormControl = new FormControl('', Validators.required);
  opciones = [
    { name: 'Indicador total fabrica' },
    { name: 'Indicador ingeniero' },
    { name: 'Historial por ingeniero' },
  ];
  constructor(
    private cabecera: CabeceraComponent,
    private indicadoresServices: IndicadoresService,
    private ingenierosServices: IngenierosService
  ) { }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this.obtenerIngenieros();
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.ingenieros.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
  obtenerIngenieros() {
    this.ingenieros = [];


    this.ingenierosServices.obtenerListaTotalIngenierosPorFecha().then((res) => {
      this.ingenieros = res.ingenieros;
    }, (e) => {
      console.log(e);
    });
  }
  obtenerResultados(objeto) {
    this.indicadoresServices.obtenerResultadoIndicadores(objeto).subscribe((data: any) => {
      this.obj = data;
      this.indicadores = data.indicadores;
      if (data.indicadores.length > 0) {
        this.mostrar = false;
        this.mostrarTabla = true;
      } else {
        this.mostrarTabla = false;
        this.mostrar = true;
      }
    }, (e) => {
      this.mostrar = true;
    });
  }
  guardarIngeniero(ingeniero) {
    this.ingeniero_sel = ingeniero;
  }

  consulta_indicadores() {
    let objeto;
    objeto = {
      obj: {
        ingeniero: this.ingeniero_sel,
        month: null,
        year: null
      }
    };

    this.obtenerResultados(objeto);
  }
  exportTable() {
    TableUtil.exportToExcel('tabla_historial', this.ingeniero_sel.substr(0, 30));
  }
}
