import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaHistorialIndicadoresComponent } from './vista-historial-indicadores.component';

describe('VistaHistorialIndicadoresComponent', () => {
  let component: VistaHistorialIndicadoresComponent;
  let fixture: ComponentFixture<VistaHistorialIndicadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaHistorialIndicadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaHistorialIndicadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
