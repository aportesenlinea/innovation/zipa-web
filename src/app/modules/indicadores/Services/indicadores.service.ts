import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class IndicadoresService {
    uri = environment.excelUrl;
    constructor(private http: HttpClient) {
    }

    addArchivoRequerimiento(archivo) {

        if (environment.authentication) {
             const httpOptions = this.obtenerHeaders();
             return this.http.post<any>(`${this.uri}/upload_reqs_file`, archivo);
        } else {
            return this.http.post<any>(`${this.uri}/upload_reqs_file`, archivo);
        }
    }
    addArchivoCapacidad(archivo) {

        if (environment.authentication) {
            const httpOptions = this.obtenerHeaders();
            return this.http.post<any>(`${this.uri}/upload_reqs_capacidad`, archivo);
        } else {
            return this.http.post<any>(`${this.uri}/upload_reqs_capacidad`, archivo);
        }
    }
    calcularIndicadores(fecha) {
        if (environment.authentication) {
            const httpOptions = this.obtenerHeaders();
            return this.http.post<any>(`${this.uri}/individual_indicators`, fecha);
        } else {
            return this.http.post<any>(`${this.uri}/individual_indicators`, fecha);
        }
    }
    obtenerResultadoIndicadores(objeto) {
        if (environment.authentication) {
            const httpOptions = this.obtenerHeaders();
            return this.http.post<any>(`${this.uri}/obtener_indicadores_tot`, objeto);
        } else {
            return this.http.post<any>(`${this.uri}/obtener_indicadores_tot`, objeto);
        }
    }
    obtenerIndicadoresDisponibles(obj) {
        if (environment.authentication) {
            const httpOptions = this.obtenerHeaders();
            return this.http.post<any>(`${this.uri}/obtener-lista-indicadores`, obj);
        } else {
            return this.http.post<any>(`${this.uri}/obtener-lista-indicadores`, obj);
        }
    }
    obtenerIndicadorUnicoIngeniero(objetoIngeniero) {
        if (environment.authentication) {
            const httpOptions = this.obtenerHeaders();
            return this.http.post<any>(`${this.uri}/obtener-indicador-unico-ingeniero`, objetoIngeniero);
        } else {
            return this.http.post<any>(`${this.uri}/obtener-indicador-unico-ingeniero`, objetoIngeniero);
        }
    }
    guardarIndicadorUnicoIngeniero(objetoIngeniero) {
        if (environment.authentication) {
            const httpOptions = this.obtenerHeaders();
            return this.http.post<any>(`${this.uri}/guardar-indicador-unico-ingeniero`, objetoIngeniero);
        } else {
            return this.http.post<any>(`${this.uri}/guardar-indicador-unico-ingeniero`, objetoIngeniero);
        }
    }
    obtenerHeaders() {
        const session = JSON.parse(localStorage.getItem('cookieFanaia'));
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                // tslint:disable-next-line: no-non-null-assertion
                Authorization: session!.token
            })
        };
        return httpOptions;
    }
}
