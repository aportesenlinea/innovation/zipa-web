import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class IngenierosService {
    uri = environment.excelUrl;
    constructor(private http: HttpClient) {
    }

    obtenerListaIngenierosPorFecha(fecha) {
        if (environment.authentication) {
            const httpOptions = this.obtenerHeaders();
            return this.http.post<any>(`${this.uri}/obtener-ingenieros`, fecha);
        } else {
            return this.http.post<any>(`${this.uri}/obtener-ingenieros`, fecha);
        }
    }
    obtenerHeaders() {
        const session = JSON.parse(localStorage.getItem('cookieFanaia'));
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                // tslint:disable-next-line: no-non-null-assertion
                Authorization: session!.token
            })
        };
        return httpOptions;
    }

    obtenerListaTotalIngenierosPorFecha() {
        if (environment.authentication) {
            const httpOptions = this.obtenerHeaders();
            return this.http.get<any>(`${this.uri}/obtener-ingenieros-total`).toPromise();
        } else {
            return this.http.get<any>(`${this.uri}/obtener-ingenieros-total`).toPromise();
        }
    }

}
