import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes, ExtraOptions } from '@angular/router';
import { TablaComponent } from './modules/cuestionarios/components/tabla/tabla.component';
import {
  MatPaginatorModule,
  MatSortModule,
  MatExpansionModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatIconModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatTableModule,
  MatDividerModule,
  MatSnackBarModule,
  MatListModule,
  MatAutocompleteModule,
  MatCheckboxModule,
  MatTooltipModule,
  MatDialogModule,
  MatMenuModule,
  MatDatepickerModule,
  MatProgressBarModule,
  MatNativeDateModule,
 } from '@angular/material';
import {MatStepperModule} from '@angular/material/stepper';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PreguntaExperienciaComponent } from './modules/cuestionarios/components/pregunta-experiencia/pregunta-experiencia.component';
import { PreguntaLenguajeComponent } from './modules/cuestionarios/components/pregunta-lenguaje/pregunta-lenguaje.component';
import { PreguntaModuloComponent } from './modules/cuestionarios/components/pregunta-modulo/pregunta-modulo.component';
import { SearchPipe } from './utils/components/search/search.pipe';
import { CuestionarioComponent } from './modules/cuestionarios/components/cuestionario/cuestionario.component';
import { CasoErrorComponent } from './modules/cuestionarios/components/caso-error/caso-error.component';
import { CasoExitoComponent } from './modules/cuestionarios/components/caso-exito/caso-exito.component';
import { InstruccionInicialComponent } from './modules/cuestionarios/components/instruccion-inicial/instruccion-inicial.component';
import { CargueExcelComponent } from './modules/indicadores/components/cargue-excel/cargue-excel.component';
import { ConfirmDialogComponent } from './modules/cuestionarios/components/confirm-dialog/confirm-dialog.component';
import { CabeceraComponent } from './utils/components/cabecera/cabecera.component';
import { SpinnerComponent } from './utils/components/spinner/spinner.component';
import {MatMomentDateModule, MomentDateAdapter} from '@angular/material-moment-adapter';
import { VistaIndicadoresComponent } from './modules/indicadores/components/vista-indicadores/vista-indicadores.component';
import { InsertarIndicadoresComponent } from './modules/indicadores/components/insertar-indicadores/insertar-indicadores.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// tslint:disable-next-line
import { VistaHistorialIndicadoresComponent } from './modules/indicadores/components/vista-historial-indicadores/vista-historial-indicadores.component';
const routes: Routes = [
  { path: 'cuestionario', component: CuestionarioComponent },
  { path: 'cuestionario/pregunta-experiencia', component: PreguntaExperienciaComponent },
  { path: 'instrucciones', component: InstruccionInicialComponent},
  { path: 'cuestionario/pregunta-lenguaje', component: PreguntaLenguajeComponent },
  { path: 'cuestionario/pregunta-modulo', component: PreguntaModuloComponent },
  { path: 'caso-error', component: CasoErrorComponent },
  { path: 'caso-exito', component: CasoExitoComponent },
  { path: 'cargue-excel', component: CargueExcelComponent },
  { path: 'tabla', component: TablaComponent },
  { path : 'vista-indicadores', component: VistaIndicadoresComponent},
  { path : 'insertar-indicadores', component: InsertarIndicadoresComponent},
  {path: 'vista-historial-indicadores', component: VistaHistorialIndicadoresComponent},
  { path: '', redirectTo: 'instrucciones', pathMatch: 'full'}

];

const routerOptions: ExtraOptions = {
  useHash: true,
  onSameUrlNavigation: 'reload',
  enableTracing: true,
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled'
};

@NgModule({
  declarations: [
    AppComponent,
    CargueExcelComponent,
    PreguntaExperienciaComponent,
    TablaComponent,
    PreguntaLenguajeComponent,
    PreguntaModuloComponent,
    SearchPipe,
    CuestionarioComponent,
    CasoErrorComponent,
    CasoExitoComponent,
    InstruccionInicialComponent,
    ConfirmDialogComponent,
    CabeceraComponent,
    SpinnerComponent,
    VistaIndicadoresComponent,
    InsertarIndicadoresComponent,
    VistaHistorialIndicadoresComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes, routerOptions),
    HttpClientModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatTableModule,
    MatDividerModule,
    MatSnackBarModule,
    MatListModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatTableModule,
    MatExpansionModule,
    NoopAnimationsModule,
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatTooltipModule,
    MatProgressBarModule,
    MatMenuModule,
    MatStepperModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatProgressSpinnerModule,
  ],
  entryComponents: [ConfirmDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
