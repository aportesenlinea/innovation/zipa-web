function BindSunburst(self_data) {
  //var self_data.self.chart;
  var _chart;

  nv.addGraph(function() {
    const vw = Math.max(
      document.documentElement.clientWidth,
      window.innerWidth || 0
    );
    const vh = Math.max(
      document.documentElement.clientHeight,
      window.innerHeight || 0
    );

    var width, height;
    width = height = (vw < vh ? vw : vh) * 0.55;

    self_data.self.chart = nv.models.sunburstChart();
    _chart = nv.models.sunburstChart();
    self_data.self.chart.margin({ left: 0, right: 0, top: 1, bottom: 1 });
    _chart.margin({ left: 0, right: 0, top: 1, bottom: 1 });

    // self_data.self.chart.color(d3.scale.category20c());
    // self_data.self.chart.groupColorByfamily(true);

    // ee32-FANAIA

    // ee33-PILA
    // ee34-Cenit
    // ee35-Voluntarias
    // ee36-Transversales
    // ee37-Servicios Financieros
    // ee38-BEPS
    // ee39-Cesantias

    function convertHex(hex) {
      var hex = hex.replace("#", "");
      var r = parseInt(hex.substring(0, 2), 16);
      var g = parseInt(hex.substring(2, 4), 16);
      var b = parseInt(hex.substring(4, 6), 16);
      var a;
      if (hex.length == 8) a = parseInt(hex.substring(7, 8), 16);

      var result = "rgba(" + r + "," + g + "," + b + "," + a + ")";
      return result;
    }

    function addOpacityToHex(hex, opacity) {
      var a = Math.round((opacity / 100) * 255).toString(16);
      if (a.length == 1) a = "0" + a;

      var result = hex + a;
      return result;
    }

    function convertToRGBpercent(hex, dark) {
      var hex = hex.replace("#", "");
      var r =
        (parseInt(hex.substring(0, 2), 16) / 255) * 100 * (dark ? 2.8 : 1);
      var g =
        (parseInt(hex.substring(2, 4), 16) / 255) * 100 * (dark ? 2.2 : 1);
      var b =
        (parseInt(hex.substring(4, 6), 16) / 255) * 100 * (dark ? 2.8 : 1);
      var a;
      if (hex.length == 8) a = parseInt(hex.substring(7, 8), 16);

      var result = "rgba(" + r + "," + g + "," + b + "," + a + ")";
      return result;
    }

    self_data.self.chart.color(d => {
      var family = d.familia;
      var color =
        family == "5e500ce7fe08372824d9ee32"
          ? "#FFF0"
          : addOpacityToHex(
              family == "5e500ce7fe08372824d9ee33"
                ? "#00805d"
                : family == "5e500ce7fe08372824d9ee34"
                ? "#fdb813"
                : family == "5e500ce7fe08372824d9ee35"
                ? "#d25934"
                : family == "5e500ce7fe08372824d9ee36"
                ? "#7fc241"
                : family == "5e500ce7fe08372824d9ee37"
                ? "#810000"
                : family == "5e500ce7fe08372824d9ee38"
                ? "#f68b1e"
                : "#D32F2F",
              d.depth == 1
                ? 100
                : d.depth == 2
                ? 80
                : d.depth == 3
                ? 60
                : d.depth == 4
                ? 40
                : 20
            );
      if (d.modulo) {
        if (d.modulo.interesa || d.modulo.conoce || d.modulo.capacitado) {
          color = "#ccc"; //convertToRGBpercent(color, true);
        }
      }
      d.color = color;
      return color;
    });

    d3.select("#modules_hide")
      .datum(getData())
      .call(_chart);

    self_data.self.treeModules = _chart.data[0];

    self_data.self.mergeModules();

    d3.select("#modules")
    .datum(getData2())
    .call(self_data.self.chart);
    
    d3.select("#modules")
      .style('width', function(d){        
        const _vw = Math.max(
          document.documentElement.clientWidth,
          window.innerWidth || 0
        );
        const _vh = Math.max(
          document.documentElement.clientHeight,
          window.innerHeight || 0
        );
        console.log('vh: ' + _vh + ', vw: ' + _vw);
        return '55' + (_vw < _vh ? 'vw' : 'vh');
      })
      .style('height', function(d){        
        const _vw = Math.max(
          document.documentElement.clientWidth,
          window.innerWidth || 0
        );
        const _vh = Math.max(
          document.documentElement.clientHeight,
          window.innerHeight || 0
        );
        console.log('vh: ' + _vh + ', vw: ' + _vw);
        return '55' + (_vw < _vh ? 'vw' : 'vh');
      });

    d3.select("#modules_hide").remove();

    self_data.self.chart.update();

    nv.utils.windowResize(e => {
      d3.select("#modules")
      .style('width', function(d){        
        const _vw = Math.max(
          document.documentElement.clientWidth,
          window.innerWidth || 0
        );
        const _vh = Math.max(
          document.documentElement.clientHeight,
          window.innerHeight || 0
        );
        console.log('vh: ' + _vh + ', vw: ' + _vw);
        return '55' + (_vw < _vh ? 'vw' : 'vh');
      })
      .style('height', function(d){        
        const _vw = Math.max(
          document.documentElement.clientWidth,
          window.innerWidth || 0
        );
        const _vh = Math.max(
          document.documentElement.clientHeight,
          window.innerHeight || 0
        );
        console.log('vh: ' + _vh + ', vw: ' + _vw);
        return '55' + (_vw < _vh ? 'vw' : 'vh');
      });
      self_data.self.chart.update();
    });

    self_data.self.chart.duration(200);

    self_data.self.chart.tooltip.keyFormatter(function(d) {
      return d.split("-")[2];
    });

    self_data.self.chart.sunburst.dispatch.elementClick = function(e) {
      self_data.click(self_data.self, e);
    };

    self_data.self.chart.tooltip.valueFormatter(function(d) {
      return "";
    });

    self_data.self.chart.showLabels(false);

    self_data.self.chart.labelFormat(function(d) {
      var name = d.name.split("-");
      var family = name[0];
      var key = name[1];
      var label = name[2];
      return "";
    });

    return self_data.self.chart;
  });

  function getData() {
    return self_data.data;
  }

  function getData2() {
    return [self_data.self.treeModules];
  }
}

module.exports = { BindSunburst };
