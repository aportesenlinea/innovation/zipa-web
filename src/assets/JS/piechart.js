function BindPieChart(self_data) {

  var arcRadius1 = [
    { inner: 0.6, outer: 1 },
    { inner: 0.65, outer: 0.95 }
  ];

  var colors = ["#00805d", "#ccc"];

  const vw = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  const vh = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

  var width, height;
  width = height = (vw < vh ? vw : vh) * 0.25;

  nv.addGraph(function() {
    var chart = nv.models
      .pieChart()
      .x(function(d) {
        return d.key;
      })
      .y(function(d) {
        return d.y;
      })
      .donut(true)
      .showLabels(false)
      .showLegend(false)
      .showTooltipPercent(false)
      .color(colors)
      .width(width)
      .height(height)
      .growOnHover(false)
      .arcsRadius(arcRadius1)
      .id("donut1"); // allow custom CSS for this one svg

    chart.title(self_data.title);

    chart.tooltip.valueFormatter(function(d) {
      return (d < 10 ? '0' : '') + d.toFixed(2) + "%";
    });

    d3.select("#" + self_data.svgId)
      .datum(self_data.data)
      .transition()
      .duration(1200)
      .attr("width", width)
      .attr("height", height)
      .call(chart);

    nv.utils.windowResize(chart.update);
    return chart;
  });
}

module.exports = { BindPieChart };
