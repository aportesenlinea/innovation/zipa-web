export const environment = {
  production: true,
  apiUrl: 'http://osaka02s:8087',
  secureUri: 'https://osaka02s:8089',
  CAUri: 'https://osaka02s:8091',
  excelUrl: 'http://osaka02s:5500',
  authentication: true
};
