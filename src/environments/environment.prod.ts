export const environment = {
  production: true,
  apiUrl: 'http://zipa-cuestionarios.westus.azurecontainer.io',
  secureUri: 'https://osaka02s:8093',
  CAUri: 'http://zipa-identidades.westus.azurecontainer.io',
  excelUrl: 'http://localhost:5500',
  authentication: false
};
