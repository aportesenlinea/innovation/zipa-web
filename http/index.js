var express = require('express');
var path = require('path');

var app = express();

app.use(express.static(path.join(__dirname + '/public')));

// viewed at http://localhost:8080
app.get('/', function(req, res) {
    res.sendFile('public/index.html');
});

var port = process.env.PORT || 8088

app.listen(port, () => console.log(`Express server running on port ${port}!`));
