FROM nginx:stable-alpine
COPY ./dist /usr/share/nginx/html
RUN ls -la /usr/share/nginx/html
VOLUME /usr/share/nginx/html
